<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>For dan Foreach</title>
</head>

<body>
    <!-- Array dengan FOR dan FOREACH -->
    <?php
    $arrWarna = array("Pink", "Red", "Teal");
    echo "Menampilkan isi array dengan FOR : <br>";
    for ($i = 0; $i < count($arrWarna); $i++) {
        echo "Warna favoritku <font color = $arrWarna[$i]>" . $arrWarna[$i] . "</font><br>";
    }

    echo "<br> Menampilkan isi array dengan FOREACH : <br>";
    foreach ($arrWarna as $warna) {
        echo " Warna favoritku <font color = $warna>" . $warna . "</font><br>";
    }
    ?>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>foreach dan while list</title>
</head>

<body>
    <!-- Array foreach dan while list -->
    <?php
    $arrNilai = array("B. Indonesia" => "90", "Matematika" => "92", "B. Inggris" => "95", "Pemrograman Web" => "93", "Algoritma Pemrograman" => "90");
    echo "Menampilkan isi array asosiatif dengan FOREACH : <br>";
    foreach ($arrNilai as $nilai => $skor) {
        echo "Nilai $nilai: $skor <br>";
    }
    ?>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array ksort() dan krsort()</title>
</head>

<body>
    <!-- Array dengan ksort() dan krsort() -->
    <?php
    $arrNilai = array("Clayton" => "70", "Ruby" => "18", "Dara" => "10", "Alfonso" => "21", "Natalie" => "25", "Kathryn" => "79", "Christopher" => "55", "Gennie" => "93");
    echo "<b> Array sebelum diurutkan  : <b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    ksort($arrNilai);
    reset($arrNilai);
    echo "<b> Array setelah diurutkan dengan ksort : <b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    krsort($arrNilai);
    reset($arrNilai);
    echo "<b> Array setelah diurutkan dengan krsort : <b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";
    ?>
</body>

</html>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array sort() dan rsort()</title>
</head>

<body>
    <!-- Array dengan sort() dan rsort() -->
    <?php
    $arrNilai = array("Clayton" => "70", "Ruby" => "18", "Dara" => "10", "Alfonso" => "21", "Natalie" => "25", "Kathryn" => "79", "Christopher" => "55", "Gennie" => "93");
    echo "<b> Array sebelum diurutkan  : <b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    sort($arrNilai);
    reset($arrNilai);
    echo "<b> Array setelah diurutkan dengan sort : <b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    rsort($arrNilai);
    reset($arrNilai);
    echo "<b> Array setelah diurutkan dengan rsort : <b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";
    ?>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array asort() dan arsort()</title>
</head>

<body>
    <!-- Array dengan asort() dan arsort() -->
    <?php
    $arrNilai = array("Clayton" => "70", "Ruby" => "18", "Dara" => "10", "Alfonso" => "21", "Natalie" => "25", "Kathryn" => "79", "Christopher" => "55", "Gennie" => "93");
    echo "<b> Array sebelum diurutkan  : <b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    asort($arrNilai);
    reset($arrNilai);
    echo "<b> Array setelah diurutkan dengan asort : <b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    arsort($arrNilai);
    reset($arrNilai);
    echo "<b> Array setelah diurutkan dengan arsort : <b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";
    ?>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fungsi tanpa return value dan parameter</title>
</head>

<body>
    <!-- Fungsi tanpa return value dan parameter -->
    <?php
    // Fungsi ini ntanpa return value dan tanpa parameter!
    function pesan()
    {
        echo "Rachel H. Pringle seorang mahasiswi Universitas Ahmad Dahlan, Rachel sedang belajar Pemrograman Web";
    }
    // Pemanggilan fungsi
    pesan();
    ?>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fungsi tanpa return value tetapi dengan parameter</title>
</head>

<body>
    <!-- Fungsi tanpa return value tetapi dengan parameter -->
    <?php
    // Fungsi ini ntanpa return value tetapi dengan parameter parameter!
    function perkenalan($nama, $salam = "Assalamualaikum")
    {
        echo $salam . ", ";
        echo "perkenalkan, nama saya " . $nama . "<br/>";
        echo "Saya sedang menempuh pendidikan S1 di Universitas Ahmad Dahlan <br/>";
    }
    // Pemanggilan fungsi
    perkenalan("Rachel H. Pringle");
    ?>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fungsi return value dan parameter</title>
</head>

<body>
    <!-- Fungsi dengan return value dan parameter -->
    <?php
    // Fungsi ini dengan return value dengan parameter parameter!
    function hitungUmur($thn_lahir, $thn_sekarang)
    {
        $umur = $thn_sekarang - $thn_lahir;
        return $umur;
    }

    // Pemanggilan fungsi
    echo "Umur saya saat ini adalah " . hitungUmur(2002, 2022) . " tahun";
    ?>
</body>

</html>