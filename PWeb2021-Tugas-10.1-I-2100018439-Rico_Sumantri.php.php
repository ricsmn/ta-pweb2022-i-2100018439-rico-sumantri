<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KONVERSI NILAI</title>
</head>

<body>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <h2 align="center">Tugas 10.1 PWEB Konversi Nilai Angka ke Huruf</h2>
        <hr style="width:50%;text-align:center;">
        <h4 align="center">Masukkan nilai angka yang ingin dikonversi</h4>
        <center>
            <label for="nilai">Nilai: </label>
            <input type="text" name="nilai" id="nilai">
            <input type="submit" value="Konversi">
            <br><br>

            <?php
            if (isset($_POST['nilai'])) {
                $nilai = $_POST['nilai'];
                if ($nilai >= 95) {
                    echo "Nilai Anda adalah A";
                } elseif ($nilai >= 90) {
                    echo "Nilai Anda adalah A-";
                } elseif ($nilai >= 85) {
                    echo "Nilai Anda adalah B+";
                } elseif ($nilai >= 80) {
                    echo "Nilai Anda adalah B";
                } elseif ($nilai >= 75) {
                    echo "Nilai Anda adalah B-";
                } elseif ($nilai >= 70) {
                    echo "Nilai Anda adalah C+";
                } elseif ($nilai >= 65) {
                    echo "Nilai Anda adalah C";
                } elseif ($nilai >= 60) {
                    echo "Nilai Anda adalah D+";
                } elseif ($nilai >= 55) {
                    echo "Nilai Anda adalah D";
                } elseif ($nilai >= 0) {
                    echo "Nilai Anda adalah E";
                } else {
                    echo "Nilai Anda E";
                }
            }
            ?>
        </center>
</body>

</html>