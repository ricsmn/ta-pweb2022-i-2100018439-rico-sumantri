<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Text dan Password</title>
</head>

<body>
    <!-- Form input type text and password -->
    <form action="" method="post" name="input">
        Daftar Anggota Basket FTI<br>
        1. <input type="text" name="anggota1"><br>
        2. <input type="text" name="anggota2"><br>
        3. <input type="text" name="anggota3"><br>
        <input type="submit" name="Input" value="Submit">
    </form>

    <?php
    // Form input type text and password
    if (isset($_POST['Input'])) {
        $anggota1 = $_POST['anggota1'];
        $anggota2 = $_POST['anggota2'];
        $anggota3 = $_POST['anggota3'];
        echo "<hr>";
        echo "Daftar Anggota Basket FTI<br>";
        echo "1. $anggota1 <br>";
        echo "2. $anggota2 <br>";
        echo "3. $anggota3 <br>";
    }
    ?>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Radio</title>
</head>

<body>
    <!-- Form input type RADIO -->
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" name="pilih">
        <h2> Pilih Jurusan</h2>
        <input type="radio" name="jurusan" value="Informatika"> Informatika <br />
        <input type="radio" name="jurusan" value="Kedokteran"> Kedokteran <br />
        <input type="radio" name="jurusan" value="Manajemen"> Manajemen <br />
        <input type="submit" name="Input" value="Pilih">
    </form>

    <?php
    // Form input type RADIO
    if (isset($_POST['jurusan'])) {
        $jurusan = $_POST['jurusan'];
        echo "Anda memilih jurusan : <b><font color='red'> $jurusan </font><b>";
    }
    ?>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CHECKBOX</title>
</head>

<body>
    <!-- Form input type CHECKBOX -->
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" name="pilih">
        <h2>Pilih Social Media yang Anda gunakan</h2>
        <input type="checkbox" name="socmed1" value="Instagram"> Instagram <br />
        <input type="checkbox" name="socmed2" value="Facebook"> Facebook <br />
        <input type="checkbox" name="socmed3" value="Twitter"> Twitter <br />
        <input type="submit" name="Input" value="Pilih">
    </form>

    <?php
    // Form input type CHECKBOX
    echo "Anda Menggunakan Social Media: <br>";
    if (isset($_POST['socmed1'])) {
        echo "- " . $_POST['socmed1'] . "<br>";
    }
    if (isset($_POST['socmed2'])) {
        echo "- " . $_POST['socmed2'] . "<br>";
    }
    if (isset($_POST['socmed3'])) {
        echo "- " . $_POST['socmed3'] . "<br>";
    }
    ?>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>COMBOBOX</title>
</head>

<body>
    <!-- Form input type COMBOBOX -->
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" name="pilih">
        <h2>Pilih Mobil:</h2>
        <select name="mobil">
            <option value="Volvo">Volvo</option>
            <option value="Toyota">Toyota</option>
            <option value="Audi">Audi</option>
            <option value="Ferarri">Ferarri</option>
        </select>
        <input type="submit" name="Pilih" value="Pilih">
    </form>

    <?php
    // Form input type COMBOBOX
    if (isset($_POST['mobil'])) {
        $mobil = $_POST['mobil'];
        echo "Anda Memilih Mobil: $mobil <br>";
    }
    ?>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TEXTAREA</title>
</head>

<body>
    <!-- Form input type TEXTAREA -->
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" name="tulis">
        <h2> Kesan & Pesan </h2>
        <textarea name="QnA" cols="40" rows="6"></textarea><br>
        <input type="submit" name="tulis" value="Submit">
    </form>

    <?php
    // Form input type TEXTAREA
    if (isset($_POST['tulis'])) {
        $QnA = nl2br($_POST['QnA']);
        echo "Terimakasih Telah Mengirimkan Pesan & Kesan<br>";
        echo "$QnA";
    }
    ?>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Validation</title>
</head>

<body>
    <!-- Form Validation -->
    <?php
    // define variables and set to empty values
    $nameErr = $emailErr = $genderErr = $websiteErr = "";
    $name = $email = $gender = $comment = $website = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
            $nameErr = "Name is required";
        } else {
            $name = test_input($_POST["name"]);
            // check if name only contains letters and whitespace
            if (!preg_match("/^[a-zA-Z-' ]*$/", $name)) {
                $nameErr = "Only letters and white space allowed";
            }
        }

        if (empty($_POST["email"])) {
            $emailErr = "Email is required";
        } else {
            $email = test_input($_POST["email"]);
            // check if e-mail address is well-formed
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailErr = "Invalid email format";
            }
        }

        if (empty($_POST["gender"])) {
            $genderErr = "Gender is required";
        } else {
            $gender = test_input($_POST["gender"]);
        }
    }

    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    ?>

    <h2>Form Validation</h2>
    <p><span class="error">*wajib diisi</span></p>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        Name: <input type="text" name="name">
        <span class="error">* <?php echo $nameErr; ?></span>
        <br><br>
        E-mail: <input type="text" name="email">
        <span class="error">* <?php echo $emailErr; ?></span>
        <br><br>
        Gender:
        <input type="radio" name="gender" value="female">Female
        <input type="radio" name="gender" value="male">Male
        <input type="radio" name="gender" value="other">Other
        <span class="error">* <?php echo $genderErr; ?></span>
        <br><br>
        <input type="submit" name="submit" value="Submit">
    </form>

    <?php
    echo "<h2>Your Input:</h2>";
    echo $name;
    echo "<br>";
    echo $email;
    echo "<br>";
    echo $gender;
    ?>
</body>

</html>